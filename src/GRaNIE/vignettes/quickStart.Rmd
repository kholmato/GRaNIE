---
title: "Get Started with the *GRaNIE* package from the Zaugg Lab"
author: "Christian Arnold, Judith Zaugg"
date: "`r doc_date()`"
package: "`r BiocStyle::pkg_ver('GRaNIE')`"
vignette: >
  %\VignetteIndexEntry{Get Started}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
output: 
  BiocStyle::html_document
---

**This vignette gives you a 1 minute summary of how to install and use our `GRaNIE` package.** 

## Install the package


Our *GRaNIE* package has been accepted on `Bioconductor`. To install, simply follow the installation instructions from the [GRaNIE Bioconductor landing page](https://bioconductor.org/packages/release/bioc/html/GRaNIE.html).

As with every Bioconductor package, there is also a *devel* version of the *GRaNIE* package. For more details, see [here](https://www.bioconductor.org/install/). 

The development version contains new features that may or may not be tested so far and we can only provide limited support for it. This is the version we actively work on, and every once in a while after finishing a new feature, we integrate it into the stable version in the `GRaNIE` package. 


## Bug Reports, Feature Requests and Contact Information

Please check out **[the main page](https://grp-zaugg.embl-community.io/GRaNIE)** for how to get in contact with us.
