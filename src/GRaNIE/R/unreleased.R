# SNP_IDs = c("rs7041847", "rs17584499", "rs55688149", "rs74340846", "rs77994054", "rs13078546", "rs765898159", "rs10788928", 
#             "rs6787151",  "rs74181299", "rs111524356", "rs12694917", "rs141033168", "rs2121266",  "rs3024493",  "rs12119474", 
#             "rs78773383", "rs75579810", "rs521734", "rs7570219",  "rs6445264",  "rs12067275" )
# res = LDlinkR::LDproxy_batch(SNP_IDs, pop = "CEU", token = "2c59a3b54f04", append = TRUE, r2d = "r2", genome_build = "grch38_high_coverage")


# either a list of SNPs as vector and automatic overlap with peaks either with or without extra LD calculation
# OR
# a data frame with manual peak-SNP associations


# plot_peaks <- function(GRN) {
#     
#     genomeAssembly = GRN@config$parameters$genomeAssembly
#     query   = .constructGRanges(GRN@data$peaks$counts_metadata, 
#                                 seqlengths = .getChrLengths(genomeAssembly), 
#                                 genomeAssembly)
#     
#     x = GenomicDistributions::calcChromBinsRef(query, genomeAssembly)
#     GenomicDistributions::plotChromBins(x)
# }

